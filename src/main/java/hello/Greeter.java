package hello;

// This is a test comment
// Test: comment

public class Greeter {

  private String name = "Edith";


  public String getName() 

  {
    return name;
  }


  public void setName(String name) 

  {
      this.name = name;
  }


  public String sayHello() 

  {
  	if (name == "Edith") 

    {
       return "Hello!";
    }
    else 
    {
       return "Hello " + name + "!";
    }

  }

}