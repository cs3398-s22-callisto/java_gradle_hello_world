package hello;

// on line change
// Comment: Morguhn Burke
// Assignment 14 comment: Morguhn Burke

// This is a comment added by btp31.
// This a comment for A14

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

// Starting class (2-3)

// Starting class (3-4)
// The comment for Assignement 9


public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty() 

   {
	g.setName("");
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello !");
   }

//    @Test
//    @DisplayName("Test for Ted")
//    public void testGreeterTed() 

//    {
//       g.setName("Ted");
//       assertEquals(g.getName(),"Ted");
//       assertEquals(g.sayHello(),"Hello Ted!");
//    }

   @Test
   @DisplayName("Test for DeJa")
   public void testGreeterTed() 
   {
      // String user = "Bill";
      g.setName("DeJa");
      assertEquals(g.getName(),"DeJa");
      assertEquals(g.sayHello(),"Hello DeJa!");
      // assertFalse(g.getName() == user);
   }

   // Assertion False Test
   @Test
   @DisplayName("Test for Falsehood")
   public void testGreeterFalse()
   {
      String user = "Bill";
      g.setName("DeJa");
      assertFalse(g.getName() == user);
   }
  
   @Test
   @DisplayName("Test for Bryce")
   public void testGreeterBryce() 

   {
      g.setName("Bryce");
      assertEquals(g.getName(),"Bryce");
      assertEquals(g.sayHello(),"Hello Bryce!");
      
   }
   
   @Test
   @DisplayName("Test for Bryce - assertionfalse")
   public void testAssertFalseBryce()
   {
   boolean falsebool = false;
   assertFalse(falsebool, "test");
   
   
   
   }
	   
   @Test
   @DisplayName("Test for Name='DeJa'")
   public void testGreeter() 
   {

      g.setName("DeJa");
      assertEquals(g.getName(),"DeJa");
      assertEquals(g.sayHello(),"Hello DeJa!");
   }

//     @Test
//    @DisplayName("Test for Name='Dude'")
//    public void testGreeter() 
//    {

//       g.setName("Dude");
//       assertEquals(g.getName(),"Dude");
//       assertEquals(g.sayHello(),"Hello DeJa!");
//    }

   @Test
   @DisplayName("Test for Morguhn")
   public void testGreeterMorguhn() 

   {
      g.setName("Morguhn");
      assertEquals(g.getName(),"Morguhn");
      assertEquals(g.sayHello(),"Hello Morguhn!");
      
   }

   @Test
   @DisplayName("Morguhn assertFalse")
   public void testAssertFalseMorguhn()
   {
     int num = 1; 
     assertFalse(num > 2);
   }

   @Test
   @DisplayName("Test for Name='Benjamin Pogue'")
   public void testGreeterBen() 
   {
      g.setName("Benjamin Pogue");
      assertEquals(g.getName(),"Benjamin Pogue");
      assertEquals(g.sayHello(),"Hello Benjamin Pogue!");
   }

   @Test
   @DisplayName("Test for character input")
   public void testGreeterCharacter() 
   {
      g.setName("\"test\"");
      assertFalse(g.getName() == "test");
   }
  
   @Test
   @DisplayName("Test2 for Bryce - assertionfalse")
   public void testAssertFalse2Bryce()
   {
   boolean falsebool = false;
   assertFalse(falsebool, "test");
   
   
   
   }
   

   @Test
   @DisplayName("Test for null")
   public void testGreeterNull()
   {
      g.setName(null);
      assertEquals(g.getName(), null);
   }

}
